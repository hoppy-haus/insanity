
```
AD [reg/num]        Add the A register to the number in the specified register
SB [reg/num]        Subtract the number in the specified register from A register
MP [reg/num]        Multiply the A register by the number in the specified register
DI [reg/num] [reg]  Divide the A register by the number in the specified register. The remainder is set to the second
                    register
LL [LV index #]     Save the current line index + 1 into the LV register
LO [reg] [LV index] Load a line number from the LV register into a main register
SV [reg] [MM index] Save a register value to the general memory register
LD [reg] [MM index] Load a value from the general memory index to the register value
JP [reg]            Move to the line in the register
CL [reg]            Set the register to 0
ST [reg] [num]      Set the selected register to the specified number
SK [reg]            Skip the amount of limes specified in the register
NI [reg]            Waits for user input of a number, then places the input in the specified register
AI [reg]            Waits for user input of an ASCII character, converts it into its ASCII number, then copies it it the
                    specified register
CA [reg]            Copies the specified register to the A register
LO [reg] [LV index] Loads the value from the LV register into the specified register
RP [reg]            Print number in the specified register
PR [reg]            Print ASCII character number in specified register
DR [reg] [index]    Writes the value in the specified register into the DR register. Index can be a register
CV [reg] [reg]      Copies the value in the first register to the second register

-- Logic Functions --
AN [reg] [reg]     (AND gate) If both registers are 1, the L register is set to 1
OR [reg] [reg]     (OR gate) If either register is 1, the L register is set to 1
NT [reg]           (NOT gate) If the register is 1, the L register is set to 0, and visa versa
NA [reg] [reg]     (NAND gate) If both registers are 1, L is set to 0. Any other values set it to 1
NO [reg] [reg]     (NOR gate) If both registers are 0, L is set to 1. Any other values set it to 0
XO [reg] [reg]     (XOR gate) if both registers are opposite, L is set to 1, equal values set it to 0

IF [reg] [num]     If the specified register is 1, don't skip the specified number of lines

```