class reg():
    v = 0
    def set(self, val):
        self.v = val

    def get(self):
        return self.v

class memoryIndex(object):
    a = 0
    def get(self):
        return self.a
    def set(self, value):
        self.a = value

class CPU(object):
    RG = {
        'A': 0,
        "B": 0,
        "C": 0,
        "D": 0,
        "E": 0,
        "F": 0,
        "G": 0,
        "H": 0,
        'L': 0,
        'N': 0,
    }
    MM = []

    for x in xrange(65535):
        MM.append(memoryIndex())
    VL = []

    for y in xrange(256):
        VL.append(memoryIndex())
    IX = 0

    DR = []
    for z in xrange(16384):
        DR.append(memoryIndex())

class functions(object):
    def __init__(self, cp):
        self.cpu = cp

    def dofunc(self, func, o1='', o2=''):
        if o1.isdigit():
            o1 = int(o1)
        if o2.isdigit():
            o2 = int(o2)
        getattr(self, func)(o1, o2)

    def PR(self, rg, a=''):
        print chr(self.cpu.RG[rg]),

    def RP(self, rg, a =''):
        print self.cpu.RG[rg],

    def AD(self, rg, a=''):
        if isinstance(rg, int):
            self.cpu.RG['A'] += rg
        else:
            self.cpu.RG['A'] += self.cpu.RG[rg]

    def SB(self, rg, a=''):
        if isinstance(rg, int):
            self.cpu.RG['A'] -= rg
        else:
            self.cpu.RG['A'] -= self.cpu.RG[rg]

    def MP(self, rg, a=''):
        if isinstance(rg, int):
            self.cpu.RG['A'] *= rg
        else:
            self.cpu.RG['A'] *= self.cpu.RG[rg]

    def DI(self, rg1, rg2):
        if isinstance(rg1, int):
            self.cpu.RG[rg2] = int(self.cpu.RG['A'] % rg1)
            self.cpu.RG['A'] = int(self.cpu.RG['A'] / rg1)
        else:
            self.cpu.RG[rg2] = int(self.cpu.RG['A'] % self.cpu.RG[rg1])
            self.cpu.RG['A'] = int(self.cpu.RG['A'] / self.cpu.RG[rg1])

    def SV(self, rg, mm):
        self.cpu.MM[mm].set(self.cpu.RG[rg])

    def LD(self, rg, mm):
        self.cpu.RG[rg] = self.cpu.MM[mm].get()

    def JP(self, rg, a=''):
        self.cpu.IX = self.cpu.RG[rg] - 2

    def CL(self, i, a=''):
        self.cpu.RG[i] = 0

    def ST(self, rg, vl):
        self.cpu.RG[rg] = vl

    def SK(self, rg, a=''):
        self.cpu.IX += self.cpu.RG[rg]

    def NI(self, rg, a=''):
        d = int(raw_input())
        self.cpu.RG[rg] = d

    def AI(self, rg, a=''):
        d = ord(raw_input()[:1])
        self.cpu.RG[rg] = d

    def CA(self, rg, a=''):
        self.cpu.RG['A'] = self.cpu.RG[rg]

    def LL(self, vl, a=''):
        self.cpu.VL[vl] = self.cpu.IX

    def LO(self, rg, i):
        self.cpu.RG[rg] = self.cpu.VL[i]

    def AN(self, rg1, rg2):
        if self.cpu.RG[rg1] == 1:
            if self.cpu.RG[rg2] == 1:
                self.cpu.RG['L'] = 1
            else:
                self.cpu.RG['L'] = 0
        else:
            self.cpu.RG['L'] = 0

    def OR(self, rg1, rg2):
        if self.cpu.RG[rg1] == 1:
            self.cpu.RG['L'] = 1
        elif self.cpu.RG[rg2] == 1:
            self.cpu.RG['L'] = 1
        else:
            self.cpu.RG['L'] = 0

    def NT(self, rg, a=''):
        if self.cpu.RG[rg] == 0:
            self.cpu.RG['L'] = 1
        elif self.cpu.RG[rg] == 1:
            self.cpu.RG['L'] = 0

    def NA(self, rg1, rg2):
        if self.cpu.RG[rg1] == 1:
            if self.cpu.RG[rg2] == 1:
                self.cpu.RG['L'] = 0
            else:
                self.cpu.RG['L'] = 1
        else:
            self.cpu.RG['L'] = 1

    def NO(self, rg1, rg2):
        if self.cpu.RG[rg1] == 0:
            if self.cpu.RG[rg2] == 0:
                self.cpu.RG['L'] = 1
            else:
                self.cpu.RG['L'] = 0
        else:
            self.cpu.RG['L'] = 0

    def XO(self, rg1, rg2):
        if self.cpu.RG[rg1] == 1:
            if self.cpu.RG[rg2] == 1:
                self.cpu.RG['L'] = 0
            else:
                self.cpu.RG['L'] = 0
        else:
            self.cpu.RG['L'] = 0

    def EX(self, c, b=''):
        exit(c)

    def IF(self, rg, l):
        if self.cpu.RG[rg] == 1:
            pass
        else:
            self.cpu.IX += l

    def DR(self, reg, i):
        if i.isdigit():
            self.cpu.DR[i] = self.cpu.RG[reg]
        else:
            self.cpu.DR[self.cpu.RG[reg]] = self.cpu.RG[reg]

    def CV(self, rg1, rg2):
        self.cpu.RG[rg1] = self.cpu.RG[rg2]



class Machine(object):
    def __init__(self, cp):
        self.cpu = cp
        self.Interpreter = functions(self.cpu)

    def start(self):
        with open('main.insasm') as f:
            instructions = f.readlines()
            while self.cpu.IX <= len(instructions) -1:
                cmd = instructions[self.cpu.IX].split(' ')
                if cmd[0] == '\r\n' or cmd[0] == '\n' or cmd[0] == '#':
                    continue
                elif len(cmd) == 2:
                    self.Interpreter.dofunc(cmd[0], cmd[1].strip())
                elif len(cmd) == 3:
                    self.Interpreter.dofunc(cmd[0], cmd[1], cmd[2].strip())
                self.cpu.IX += 1

if __name__ == '__main__':
    cp = CPU()
    Machine(cp).start()